/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package prgt3e20aldarias;

/**
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 28-oct-2013
 */

class Pajaro20 {
//*** atributos o propiedades ****
  private char color; //propiedad o atributo color
  private int edad; //propiedad o atributo edad
//*** metodos de la clase ****
  public void setedad(int e) {
    edad = e;
  }
  public void printedad() {
    System.out.println(edad);
  }
  public void setcolor(char c) {
    color=c;
  }
  public void printcolor() {
    switch(color) {
//Los pajaros son verdes, amarillos, grises, negros o blancos
//No existen pájaros de otros colores
    case 'v':
      System.out.println("verde");
      break;
    case 'a':
      System.out.println("amarillo");
      break;
    case 'g':
      System.out.println("gris");
      break;
    case 'n':
      System.out.println("negro");
      break;
    case 'b':
      System.out.println("blanco");
      break;
    default:
      System.out.println("color no establecido");
    }
  }
}

class Pajaro12 {
  public static void main(String[] args) {
    Pajaro20 p=new Pajaro20();
    // p.edad=5; // Error;
    p.setedad(5);
    p.printedad();
  }
}