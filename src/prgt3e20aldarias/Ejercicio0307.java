/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt3e20aldarias;

/**
 * Fichero: Ejercicio0307.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 28-oct-2013
 */
public class Ejercicio0307 {

  int dato;

  Ejercicio0307(int d) {
    dato = d;
  }

  public int doble() {
    return dato + dato;
  }

  public int triple() {
    return doble() + dato;
  }

  public int cuadruple() {
    return triple() + dato;
  }

  public static void main(String[] args) {

    Ejercicio0307 m = new Ejercicio0307(5);
    System.out.println(m.doble());
    System.out.println(m.triple());
    System.out.println(m.cuadruple());
  }
}
/* EJECUCION:
10
15
20
*/
