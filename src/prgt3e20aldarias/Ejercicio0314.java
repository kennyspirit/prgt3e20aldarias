/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package prgt3e20aldarias;

/**
 * Fichero: Ejercicio0314.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 29-oct-2013
 */

public class Ejercicio0314 {
double kms;
  double litros;
  double vmed;
  double pgas;

  Ejercicio0314() {
    kms=litros=vmed=100;
    pgas=1.50;
  }
  public double getTiempo() { //devuelve el tiempo en horas
    return kms/vmed;
  };
  public double consumoMedio() { //litros cada 100 kms
    return (litros*100)/kms;
  }
  public double consumoEuros() {
    return litros*pgas;
  }

  public void setKms(double k) {
    kms=k;
  }
  public void setLitros(double l) {
    litros=l;
  }
  public void setVmed(double v) {
    vmed=v;
  }
  public void setPgas(double p) {
    pgas=p;
  }

  public String show() {
    return "kms: "+kms+" l:"+litros+" v:"+vmed+" consumo: "+pgas
           +" consumoMedio: "+consumoMedio()+" tiempo:"+getTiempo();
  }

  public static void main(String [] args) {
    Ejercicio0314 c = new Ejercicio0314();
    c.setKms(1000);
    c.setLitros(100);
    c.setVmed(100);
    c.setPgas(100);
    System.out.println(c.show());
  }
}
/* EJECUCION:
kms: 1000.0 l:100.0 v:100.0 consumo: 100.0 consumoMedio: 10.0 tiempo:10.0
*/
