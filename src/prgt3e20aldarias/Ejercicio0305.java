package prgt3e20aldarias;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Fichero: Ejercicio0305.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 28-oct-2013
 */
public class Ejercicio0305 {

  public static void main(String[] args) throws IOException {
    int num1, num2, cociente;
    InputStreamReader input = new InputStreamReader(System.in);
    BufferedReader buffer = new BufferedReader(input);
    String linea;

    try {
      System.out.print("Introduce Dividendo: ");
      linea = buffer.readLine();
      num1 = Integer.parseInt(linea);
      System.out.print("Introduce Divisor: ");
      linea = buffer.readLine();
      num2 = Integer.parseInt(linea);
      cociente = num1 / num2;
      System.out.println("El cociente es: " + cociente);
    } catch ( ArithmeticException | IOException e) {
      System.err.println("Error " + e.getMessage());
    }
  }
}

/* EJECUCION:
 Introduce Dividendo: 2
 Introduce Divisor: 0
 Error: / by zero
 */
